package graphql

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"errors"
	"fmt"
	"github.com/go-pg/pg/v9/orm"
	"log"

	"bitbucket.org/ZackArts/blog-backend/graphql/generated"
	"bitbucket.org/ZackArts/blog-backend/middleware"
	"bitbucket.org/ZackArts/blog-backend/models"
)

func (r *mutationResolver) Register(ctx context.Context, input *models.RegisterInput) (*models.AuthResponse, error) {
	_, err := r.UsersRepo.GetUserByEmail(input.Email)
	if err == nil {
		return nil, errors.New("email already in use")
	}

	_, err = r.UsersRepo.GetUserByUsername(input.Username)
	if err == nil {
		return nil, errors.New("username already taken")
	}

	user := &models.User{
		Username: input.Username,
		Email:    input.Email,
	}

	err = r.UsersRepo.DB.CreateTable(user, &orm.CreateTableOptions{IfNotExists: true})
	if err != nil {
		log.Printf("error while creating table: %v", err)
	}

	err = user.HashPassword(input.Password)
	if err != nil {
		log.Printf("error while hasing password %v", err)
	}
	// TODO: send verification code

	if err != nil {
		log.Printf("error creating a table : %v", err)
	}
	tx, err := r.UsersRepo.DB.Begin()
	if err != nil {
		log.Printf("error creating a transaction: %v", err)
		return nil, errors.New("something went wrong")
	}
	defer tx.Rollback()
	if _, err := r.UsersRepo.CreateUser(tx, user); err != nil {
		log.Printf("error creating a transaction %v", err)
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		log.Printf("error while committing: %v", err)
		return nil, err
	}

	token, err := user.GenToken()
	if err != nil {
		log.Printf("error while generationg the token %v", err)
		return nil, errors.New("something went wrong")
	}

	return &models.AuthResponse{
		AuthToken: token,
		User:      user,
	}, nil
}

func (r *mutationResolver) Login(ctx context.Context, input *models.LoginInput) (*models.AuthResponse, error) {
	user, err := r.UsersRepo.GetUserByEmail(input.Email)
	if err != nil {
		return nil, ErrBadCredentials
	}

	err = user.ComparePassword(input.Password)
	if err != nil {
		return nil, ErrBadCredentials
	}

	token, err := user.GenToken()
	if err != nil {
		return nil, ErrBadCredentials
	}

	return &models.AuthResponse{
		AuthToken: token,
		User:      user,
	}, nil
}

func (r *mutationResolver) CreatePost(ctx context.Context, input models.NewPost) (*models.Post, error) {
	currentUser, err := middleware.GetCurrentUserFromCTX(ctx)
	if err != nil {
		return nil, ErrUnauthenticated
	}

	if len(input.Name) < 3 {
		return nil, errors.New("name not long enough")
	}

	if len(input.Body) < 3 {
		return nil, errors.New("body not long enough")
	}

	post := &models.Post{
		Name:   input.Name,
		Body:   input.Body,
		UserID: currentUser.ID,
	}

	err = r.PostsRepo.DB.CreateTable(post, &orm.CreateTableOptions{IfNotExists: true})

	return r.PostsRepo.CreatePost(post)
}

func (r *mutationResolver) UpdatePost(ctx context.Context, id string, input models.UpdatePost) (*models.Post, error) {
	post, err := r.PostsRepo.GetByID(id)
	if err != nil || post == nil {
		return nil, errors.New("post does not exist")
	}

	didUpdate := false

	if input.Name != nil {
		if len(*input.Name) < 3 {
			return nil, errors.New("name is not long enough")
		}
		post.Name = *input.Name
		didUpdate = true
	}

	if input.Body != nil {
		if len(*input.Body) < 3 {
			return nil, errors.New("body is not long enough")
		}
		post.Body = *input.Body
		didUpdate = true
	}

	if !didUpdate {
		return nil, errors.New("no update done")
	}

	post, err = r.PostsRepo.Update(post)
	if err != nil {
		return nil, fmt.Errorf("error while updating place %v", err)
	}
	return post, nil
}

func (r *mutationResolver) DeletePost(ctx context.Context, id string) (bool, error) {
	post, err := r.PostsRepo.GetByID(id)
	if err != nil || post == nil {
		return false, errors.New("post does not exist")
	}

	err = r.PostsRepo.Delete(post)
	if err != nil {
		return false, fmt.Errorf("error while deleting place %v", err)
	}

	return true, err
}

func (r *mutationResolver) CreateComment(ctx context.Context, input models.NewComment) (*models.Comment, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *mutationResolver) UpdateComment(ctx context.Context, id string, input models.UpdateComment) (*models.Comment, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *mutationResolver) DeleteComment(ctx context.Context, id string) (bool, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *mutationResolver) DeleteUser(ctx context.Context, id string) (bool, error) {
	user, err := r.UsersRepo.GetUserById(id)
	if err != nil || user == nil {
		return false, errors.New("user does not exist")
	}

	err = r.UsersRepo.Delete(user)
	if err != nil {
		return false, fmt.Errorf("error while deleting place %v", err)
	}

	return true, err
}

func (r *queryResolver) Users(ctx context.Context) ([]*models.User, error) {
	return r.UsersRepo.GetUsers()
}

func (r *queryResolver) Places(ctx context.Context, filter *models.PostFilter, limit *int, offset *int) ([]*models.Post, error) {
	return r.PostsRepo.GetPosts(filter, limit, offset)
}

func (r *queryResolver) User(ctx context.Context, id string) (*models.User, error) {
	return r.UsersRepo.GetUserById(id)
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
