//go:generate go run github.com/99designs/gqlgen

package graphql

import "bitbucket.org/ZackArts/blog-backend/postgres"

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct {
	PostsRepo postgres.PostsRepo
	UsersRepo postgres.UsersRepo
}
