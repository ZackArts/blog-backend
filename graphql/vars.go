package graphql

import "errors"

var (
	ErrBadCredentials  = errors.New("email/password combination didn't work")
	ErrUnauthenticated = errors.New("unauthenticated")
)
