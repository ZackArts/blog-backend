# blog-backend

graphql backend for my blog. connects to a postgresql database with environment variables.

## Works:
- User login/signup
- Post creation/deletion
- Authorization via jwt

## Doesn't work/unimplemented
- Comments
- Getting posts from users

