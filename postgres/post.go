package postgres

import (
	"bitbucket.org/ZackArts/blog-backend/models"
	"fmt"
	"github.com/go-pg/pg/v9"
)

type PostsRepo struct {
	DB *pg.DB
}

func (p *PostsRepo) GetPosts(filter *models.PostFilter, limit, offset *int) ([]*models.Post, error) {
	var posts []*models.Post

	query := p.DB.Model(&posts).Order("id")

	if filter != nil {
		if filter.Name != nil && *filter.Name != "" {
			query.Where("name ILIKE ?", fmt.Sprintf("%%%s%%", *filter.Name))
		}
	}

	if limit != nil {
		query.Limit(*limit)
	}

	if offset != nil {
		query.Offset(*offset)
	}

	err := query.Select()
	if err != nil {
		return nil, err
	}

	return posts, nil
}

func (p *PostsRepo) CreatePost(post *models.Post) (*models.Post, error) {
	_, err := p.DB.Model(post).Returning("*").Insert()

	return post, err
}

func (p *PostsRepo) GetByID(id string) (*models.Post, error) {
	var post models.Post
	err := p.DB.Model(&post).Where(" id = ?", id).First()
	return &post, err
}

func (p *PostsRepo) Update(post *models.Post) (*models.Post, error) {
	_, err := p.DB.Model(post).Where("id = ?", post.ID).Update()
	return post, err
}

func (p *PostsRepo) Delete(post *models.Post) error {
	_, err := p.DB.Model(post).Where("id = ?", post.ID).Delete()
	return err
}

func (p *PostsRepo) GetPostsForUser(user *models.User) ([]*models.Post, error) {
	var posts []*models.Post
	err := p.DB.Model(&posts).Where("user_id = ?", user.ID).Order("id").Select()
	return posts, err
}
